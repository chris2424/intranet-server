<?php

 $LocaleInfo['vf_id'] = array (
  'Locale' => 'id',
  'Name' => 'Indonesia / Indonesian',
  'EnName' => 'Indonesian',
  'Description' => 'Official Indonesian language translations for Vanilla. Help contribute to this translation by going to its translation site <a href="https://www.transifex.com/projects/p/vanilla/language/id/">here</a>.',
  'Version' => '2018.11.19p1011',
  'Author' => 'Vanilla Community',
  'AuthorUrl' => 'https://www.transifex.com/projects/p/vanilla/language/id/',
  'License' => 'CC BY-SA 4.0',
  'PercentComplete' => 98,
  'NumComplete' => 2367,
  'DenComplete' => 2425,
  'Icon' => 'id.svg',
);
