<?php if (!defined('APPLICATION')) exit();

// Conversations
$Configuration['Conversations']['Version'] = '3.0';

// Database
$Configuration['Database']['Name'] = 'bnpbcenter';
$Configuration['Database']['Host'] = 'localhost';
$Configuration['Database']['User'] = 'root';
$Configuration['Database']['Password'] = '';

// EnabledApplications
$Configuration['EnabledApplications']['Conversations'] = 'conversations';
$Configuration['EnabledApplications']['Vanilla'] = 'vanilla';

// EnabledLocales
$Configuration['EnabledLocales']['vf_id'] = 'id';

// EnabledPlugins
$Configuration['EnabledPlugins']['recaptcha'] = false;
$Configuration['EnabledPlugins']['GettingStarted'] = 'GettingStarted';
$Configuration['EnabledPlugins']['stubcontent'] = true;
$Configuration['EnabledPlugins']['swagger-ui'] = true;
$Configuration['EnabledPlugins']['Quotes'] = true;
$Configuration['EnabledPlugins']['rich-editor'] = true;
$Configuration['EnabledPlugins']['Facebook'] = false;
$Configuration['EnabledPlugins']['Twitter'] = false;
$Configuration['EnabledPlugins']['editor'] = true;
$Configuration['EnabledPlugins']['AllViewed'] = true;
$Configuration['EnabledPlugins']['IndexPhotos'] = true;
$Configuration['EnabledPlugins']['emojiextender'] = true;
$Configuration['EnabledPlugins']['Flagging'] = true;
$Configuration['EnabledPlugins']['heroimage'] = true;
$Configuration['EnabledPlugins']['VanillaStats'] = true;
$Configuration['EnabledPlugins']['VanillaInThisDiscussion'] = true;
$Configuration['EnabledPlugins']['PlayMyMP3'] = true;
$Configuration['EnabledPlugins']['LiveStreamPage'] = false;
$Configuration['EnabledPlugins']['EventCalendar'] = false;
$Configuration['EnabledPlugins']['Galleries'] = true;
$Configuration['EnabledPlugins']['BirthdayModule'] = true;
$Configuration['EnabledPlugins']['ProfileExtender'] = true;
$Configuration['EnabledPlugins']['linestickers'] = false;
$Configuration['EnabledPlugins']['UsersOnline'] = true;
$Configuration['EnabledPlugins']['googlesignin'] = false;

// EventCalendar
$Configuration['EventCalendar']['CustomRoute'] = 'eventcalendar';
$Configuration['EventCalendar']['CategoryIDs'] = 'a:3:{i:0;s:1:"4";i:1;s:1:"3";i:2;s:1:"2";}';

// Garden
$Configuration['Garden']['Title'] = 'BNPB Coordination Center';
$Configuration['Garden']['Cookie']['Salt'] = 'bPoU1jTSe4W7Q5Bo';
$Configuration['Garden']['Cookie']['Domain'] = '';
$Configuration['Garden']['Registration']['ConfirmEmail'] = '1';
$Configuration['Garden']['Registration']['Method'] = 'Approval';
$Configuration['Garden']['Registration']['InviteExpiration'] = '1 week';
$Configuration['Garden']['Registration']['InviteTarget'] = '';
$Configuration['Garden']['Registration']['InviteRoles'][3] = '0';
$Configuration['Garden']['Registration']['InviteRoles'][4] = '0';
$Configuration['Garden']['Registration']['InviteRoles'][8] = '0';
$Configuration['Garden']['Registration']['InviteRoles'][16] = '0';
$Configuration['Garden']['Registration']['InviteRoles'][32] = '0';
$Configuration['Garden']['Registration']['SendConnectEmail'] = '1';
$Configuration['Garden']['Email']['SupportName'] = 'BNPB Command Center';
$Configuration['Garden']['Email']['Format'] = 'html';
$Configuration['Garden']['Email']['SupportAddress'] = 'webbnpb@gmail.com';
$Configuration['Garden']['Email']['UseSmtp'] = '1';
$Configuration['Garden']['Email']['SmtpHost'] = 'smtp.gmail.com';
$Configuration['Garden']['Email']['SmtpUser'] = 'krisnarahmat1@gmail.com';
$Configuration['Garden']['Email']['SmtpPassword'] = '123453131';
$Configuration['Garden']['Email']['SmtpPort'] = '465';
$Configuration['Garden']['Email']['SmtpSecurity'] = 'ssl';
$Configuration['Garden']['Email']['OmitToName'] = false;
$Configuration['Garden']['SystemUserID'] = '1';
$Configuration['Garden']['InputFormatter'] = 'Rich';
$Configuration['Garden']['Version'] = 'Undefined';
$Configuration['Garden']['CanProcessImages'] = true;
$Configuration['Garden']['MobileInputFormatter'] = 'Wysiwyg';
$Configuration['Garden']['Installed'] = true;
$Configuration['Garden']['HomepageTitle'] = 'BNPB Coordination Center';
$Configuration['Garden']['Description'] = 'BNPB Coordination Center';
$Configuration['Garden']['Logo'] = 'd9817a8822e75b3c3b8831e7806a6353.png';
$Configuration['Garden']['MobileLogo'] = '73a8cd1427bb8f39ab476f1cc96a9ae6.png';
$Configuration['Garden']['FavIcon'] = 'favicon_e038bd82b32466b5ad2d0eca008a098b.ico';
$Configuration['Garden']['TouchIcon'] = 'favicon-152-cd4c69e6a4bf5dd8a9276d4d186ad03f.png';
$Configuration['Garden']['ShareImage'] = '8c9bbbe32e9f92aa4035c88c659caedd.png';
$Configuration['Garden']['MobileAddressBarColor'] = '#0080ff';
$Configuration['Garden']['DefaultAvatar'] = 'defaultavatar/4PNHXFZMAG8W.jpg';
$Configuration['Garden']['EmailTemplate']['Image'] = '41fc61a9d49d2c079ba7ec6a63c1928e.png';
$Configuration['Garden']['EmailTemplate']['TextColor'] = '#333333';
$Configuration['Garden']['EmailTemplate']['BackgroundColor'] = '#eeeeee';
$Configuration['Garden']['EmailTemplate']['ContainerBackgroundColor'] = '#ffffff';
$Configuration['Garden']['EmailTemplate']['ButtonTextColor'] = '#ffffff';
$Configuration['Garden']['EmailTemplate']['ButtonBackgroundColor'] = '#38abe3';
$Configuration['Garden']['PrivateCommunity'] = true;
$Configuration['Garden']['Embed']['Allow'] = true;
$Configuration['Garden']['Analytics']['AllowLocal'] = true;
$Configuration['Garden']['InstallationID'] = '';
$Configuration['Garden']['InstallationSecret'] = '';
$Configuration['Garden']['AllowFileUploads'] = true;
$Configuration['Garden']['HeroImage'] = 'f503424eb1e4647d78d824243b9334c9.png';
$Configuration['Garden']['Theme'] = 'keystone';
$Configuration['Garden']['MobileTheme'] = 'theme-boilerplate';
$Configuration['Garden']['EditContentTimeout'] = '3600';
$Configuration['Garden']['Format']['DisableUrlEmbeds'] = false;
$Configuration['Garden']['Format']['Hashtags'] = false;
$Configuration['Garden']['ThemeOptions']['Styles']['Key'] = 'Default';
$Configuration['Garden']['ThemeOptions']['Styles']['Value'] = '%s_default';
$Configuration['Garden']['ThemeOptions']['Options']['hasHeroBanner'] = false;
$Configuration['Garden']['ThemeOptions']['Options']['hasFeatureSearchbox'] = false;
$Configuration['Garden']['ThemeOptions']['Options']['panelToLeft'] = true;
$Configuration['Garden']['EmojiSet'] = 'rice';
$Configuration['Garden']['Locale'] = 'id';

// ImageUpload
$Configuration['ImageUpload']['Limits']['Width'] = '1000';
$Configuration['ImageUpload']['Limits']['Height'] = '1400';
$Configuration['ImageUpload']['Limits']['Enabled'] = false;

// Plugin
$Configuration['Plugin']['UsersOnline']['ExpLength'] = 'num10';
$Configuration['Plugin']['UsersOnline']['MsgTxt'] = 'users online';

// Plugins
$Configuration['Plugins']['GettingStarted']['Dashboard'] = '1';
$Configuration['Plugins']['GettingStarted']['Registration'] = '1';
$Configuration['Plugins']['GettingStarted']['Plugins'] = '1';
$Configuration['Plugins']['GettingStarted']['Discussion'] = '1';
$Configuration['Plugins']['GettingStarted']['Profile'] = '1';
$Configuration['Plugins']['Facebook']['ApplicationID'] = '';
$Configuration['Plugins']['Facebook']['Secret'] = '';
$Configuration['Plugins']['Facebook']['UseFacebookNames'] = false;
$Configuration['Plugins']['Facebook']['SocialSignIn'] = '1';
$Configuration['Plugins']['Facebook']['SocialReactions'] = '1';
$Configuration['Plugins']['editor']['ForceWysiwyg'] = '1';

// ProfileExtender
$Configuration['ProfileExtender']['Fields']['DateOfBirth']['FormType'] = 'DateOfBirth';
$Configuration['ProfileExtender']['Fields']['DateOfBirth']['Label'] = 'DateOfBirth';
$Configuration['ProfileExtender']['Fields']['DateOfBirth']['Options'] = '';
$Configuration['ProfileExtender']['Fields']['DateOfBirth']['Required'] = '1';
$Configuration['ProfileExtender']['Fields']['DateOfBirth']['OnRegister'] = '1';
$Configuration['ProfileExtender']['Fields']['DateOfBirth']['OnProfile'] = '1';

// Recaptcha
$Configuration['Recaptcha']['PrivateKey'] = '';
$Configuration['Recaptcha']['PublicKey'] = '';

// RichEditor
$Configuration['RichEditor']['Quote']['Enable'] = '1';

// Routes
$Configuration['Routes']['YXBwbGUtdG91Y2gtaWNvbi5wbmc='] = array (
  0 => 'utility/showtouchicon',
  1 => 'Internal',
);
$Configuration['Routes']['cm9ib3RzLnR4dA=='] = array (
  0 => '/robots',
  1 => 'Internal',
);
$Configuration['Routes']['dXRpbGl0eS9yb2JvdHM='] = array (
  0 => '/robots',
  1 => 'Internal',
);
$Configuration['Routes']['Y29udGFpbmVyLmh0bWw='] = array (
  0 => 'staticcontent/container',
  1 => 'Internal',
);
$Configuration['Routes']['DefaultController'] = array (
  0 => 'categories',
  1 => 'Internal',
);

// Tagging
$Configuration['Tagging']['Discussions']['Enabled'] = true;

// Vanilla
$Configuration['Vanilla']['Version'] = '3.0';
$Configuration['Vanilla']['Discussions']['Layout'] = 'table';
$Configuration['Vanilla']['Discussions']['PerPage'] = '30';
$Configuration['Vanilla']['Categories']['Layout'] = 'mixed';
$Configuration['Vanilla']['Categories']['MaxDisplayDepth'] = '3';
$Configuration['Vanilla']['Comments']['PerPage'] = '30';
$Configuration['Vanilla']['Comment']['MaxLength'] = '8000';
$Configuration['Vanilla']['Comment']['MinLength'] = '';
$Configuration['Vanilla']['AdminCheckboxes']['Use'] = '1';
$Configuration['Vanilla']['Password']['SpamCount'] = 2;
$Configuration['Vanilla']['Password']['SpamTime'] = 1;
$Configuration['Vanilla']['Password']['SpamLock'] = 120;

// Last edited by adminBNPB (192.168.80.30) 2019-11-25 08:03:41