<!DOCTYPE html>
<html lang="{$CurrentLocale.Key}">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    {asset name="Head"}
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,400i,700,700i" rel="stylesheet">
</head>

{assign
    "linkFormat"
    "<div class='Navigation-linkContainer'>
        <a href='%url' class='Navigation-link %class'>
            %text
        </a>
    </div>"
}
{assign var="SectionGroups" value=(isset($Groups) || isset($Group))}

<body id="{$BodyID}" class="
    {$BodyClass}

    {if $ThemeOptions.Options.hasHeroBanner}
        ThemeOptions-hasHeroBanner
    {/if}

    {if $ThemeOptions.Options.hasFeatureSearchbox}
        ThemeOptions-hasFeatureSearchbox
    {else}
        hideHomepageTitle
    {/if}

    {if $ThemeOptions.Options.panelToLeft}
        ThemeOptions-panelToLeft
    {/if}

    {if $User.SignedIn}
        UserLoggedIn
    {else}
        UserLoggedOut
    {/if}

    {if inSection('Discussion') and $Page gt 1}
        isNotFirstPage
    {/if}

    {if inSection('Group') && !isset($Group.Icon)}
        noGroupIcon
    {/if}

    locale-{$CurrentLocale.Lang}
">

    <!--[if lt IE 9]>
      <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->

    <div class="Frame" id="page">
        <div class="Frame-top">
            <div class="Frame-header">

                <!---------- Main Header ---------->
                <header id="MainHeader" class="Header">
                    <div class="Container">
                        <div class="row">
                            <div class="Hamburger">
                                <button class="Hamburger Hamburger-menuXcross" id="menu-button" aria-label="toggle menu">
                                    <span class="Hamburger-menuLines" aria-hidden="true">
                                    </span>
                                    <span class="Hamburger-visuallyHidden sr-only">
                                        toggle menu
                                    </span>
                                </button>
                            </div>
                            <a href="{home_link format="%url"}" class="Header-logo">
                                {logo}
                            </a>
                            <a href="{home_link format="%url"}" class="Header-logo mobile">
                                {mobile_logo}
                            </a>
                            <nav class="Header-desktopNav">
                                {categories_link format=$linkFormat}
                                {discussions_link format=$linkFormat}
                                {custom_menu format=$linkFormat}
                            </nav>
                            <div class="Header-flexSpacer"></div>
                            <div class="Header-right">
                                <div class="MeBox-header">
                                    {module name="MeModule" CssClass="FlyoutRight"}
                                </div>
                                {if $User.SignedIn}
                                    <button class="mobileMeBox-button">
                                        <span class="Photo PhotoWrap">
                                            <img src="{$User.Photo|escape:'html'}" class="ProfilePhotoSmall" alt="{t c='Avatar'}">
                                        </span>
                                    </button>
                                {/if}
                            </div>
                        </div>
                    </div>

                    <!---------- Mobile Navigation ---------->
                    <nav class="Navigation js-nav needsInitialization">
                        <div class="Container">
                            {if $User.SignedIn}
                                <div class="Navigation-row NewDiscussion">
                                    <div class="NewDiscussion mobile">
                                        {module name="NewDiscussionModule"}
                                    </div>
                                </div>
                            {else}
                                <div class="Navigation-row">
                                    <div class="SignIn mobile">
                                        {module name="MeModule"}
                                    </div>
                                </div>
                            {/if}
                            {categories_link format=$linkFormat}
                            {discussions_link format=$linkFormat}
                            {activity_link format=$linkFormat}
                            {custom_menu format=$linkFormat}
                        </div>
                    </nav>
                    <nav class="mobileMebox js-mobileMebox needsInitialization">
                        <div class="Container">
                            {module name="MeModule"}
                            <button class="mobileMebox-buttonClose Close">
                                <span>×</span>
                            </button>
                        </div>
                    </nav>
                    <!---------- Mobile Navigation END ---------->

                </header>
                <!---------- Main Header END ---------->

            </div>
            <div class="Frame-body">

                <!---------- Hero Banner ---------->
                {if $ThemeOptions.Options.hasHeroBanner && inSection(["CategoryList", "DiscussionList"])}
                    <div class="Herobanner">
                        {if $heroImageUrl}
                            <div class="Herobanner-bgImage" style="background-image:url('{$heroImageUrl}')"></div>
                        {/if}
                        <div class="Container">
                            {if $ThemeOptions.Options.hasFeatureSearchbox}
                                <div class="SearchBox js-sphinxAutoComplete" role="search">
                                    {if $hasAdvancedSearch === true}
                                        {module name="AdvancedSearchModule"}
                                    {else}
                                        {searchbox}
                                    {/if}
                                </div>
                            {else}
                                {if $Category}
                                    <h2 class="H HomepageTitle">{$Category.Name}{follow_button}</h2>
                                    <p class="P PageDescription">{$Category.Description}</p>
                                {else}
                                    {if {homepage_title} !== ""}
                                        <h2 class="H HomepageTitle">{homepage_title}</h2>
                                    {/if}
                                    {if $_Description}
                                        <p class="P PageDescription">{$_Description}</p>
                                    {/if}
                                {/if}
                            {/if}
                        </div>
                    </div>
                {/if}
                <!---------- Hero Banner END ---------->

                <div class="Frame-content">
                    <div class="Container">
                        <div class="Frame-contentWrap">
                            <div class="Frame-details">
                                {if !$isHomepage}
                                    <div class="Frame-row">
                                        <nav class="BreadcrumbsBox">
                                            {breadcrumbs}
                                        </nav>
                                    </div>
                                {/if}
                                <div class="Frame-row SearchBoxMobile">
                                    {if !$SectionGroups && !inSection(["SearchResults"])}
                                        <div class="SearchBox js-sphinxAutoComplete" role="search">
                                            {if $hasAdvancedSearch === true}
                                                {module name="AdvancedSearchModule"}
                                            {else}
                                                {searchbox}
                                            {/if}
                                        </div>
                                    {/if}
                                </div>
                                <div class="Frame-row">

                                    <!---------- Main Content ---------->
                                    <main class="Content MainContent">
                                        <!---------- Profile Page Header ---------->
                                        {if inSection("Profile")}
                                            <div class="Profile-header">
                                                <div class="Profile-photo">
                                                    <div class="PhotoLarge">
                                                        {module name="UserPhotoModule"}
                                                    </div>
                                                </div>
                                                <div class="Profile-name">
                                                    <div class="Profile-row">
                                                        <h1 class="Profile-username">
                                                            {$Profile.Name}
                                                        </h1>
                                                    </div>
                                                    <div class="Profile-row">
                                                        {if isset($Rank)}
                                                            <span class="Profile-rank">{$Rank.Label}</span>
                                                        {/if}
                                                    </div>
                                                </div>
                                            </div>
                                        {/if}
                                        <!---------- Profile Page Header END ---------->

                                        {asset name="Content"}
                                    </main>
                                    <!---------- Main Content END ---------->

                                    <!---------- Main Panel ---------->
                                    <aside class="Panel Panel-main">
                                        {if !$SectionGroups}
                                            <div class="SearchBox js-sphinxAutoComplete" role="search">
                                                {searchbox}
                                            </div>
                                        {/if}
                                        {asset name="Panel"}
                                    </aside>
                                    <!---------- Main Panel END ---------->

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="Frame-footer">

            <!---------- Main Footer END ---------->
            <footer class="Footer">
                <div class="Container">
                    <div class="row">
                        <div class="col">
                            <p class="Footer-copyright">{t c="© BNPB"} {$smarty.now|date_format:"%Y"}</p>
                        </div>
                        <div class="col">
                             <div class="Vanilla-logo">
                            <a href="https://bnpb.go.id"><img  class="img-fluid" width="100" src="http://perpustakaan.bnpb.go.id/template/bnpblib/assets/img/bnpb-footer-logo.png" alt=""></a> 
                        </div>
                        </div>
                    </div>
                    {asset name="Foot"}
                </div>
            </footer>
            <!---------- Main Footer END ---------->

        </div>
    </div>
    <div id="modals"></div>
    {event name="AfterBody"}
</body>

</html>
