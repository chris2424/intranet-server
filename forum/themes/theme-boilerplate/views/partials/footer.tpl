<footer class="Footer">
    <div class="Container">
        <div class="row">
            <div class="col">
                <p class="Footer-copyright">{t c="© Pusat Data Informasi Dan Humas"} {$smarty.now|date_format:"%Y"}</p>
            </div>
            <div class="col">
                <div class="Vanilla-logo">
                            <a href="https://bnpb.go.id"><img  class="img-fluid" width="100" src="http://perpustakaan.bnpb.go.id/template/bnpblib/assets/img/bnpb-footer-logo.png" alt=""></a> 
                        </div>
            </div>
        </div>
        {asset name="Foot"}
    </div>
</footer>
