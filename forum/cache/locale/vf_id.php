<?php return Vanilla\Addon::__set_state(array(
   'info' => 
  array (
    'locale' => 'id',
    'name' => 'Indonesia / Indonesian',
    'enName' => 'Indonesian',
    'description' => 'Official Indonesian language translations for Vanilla. Help contribute to this translation by going to its translation site <a href="https://www.transifex.com/projects/p/vanilla/language/id/">here</a>.',
    'version' => '2018.11.19p1011',
    'license' => 'CC BY-SA 4.0',
    'percentComplete' => 98,
    'numComplete' => 2367,
    'denComplete' => 2425,
    'icon' => 'id.svg',
    'key' => 'vf_id',
    'type' => 'locale',
    'priority' => 11,
    'authors' => 
    array (
      0 => 
      array (
        'name' => 'Vanilla Community',
        'homepage' => 'https://www.transifex.com/projects/p/vanilla/language/id/',
      ),
    ),
    'Issues' => 
    array (
    ),
  ),
   'classes' => 
  array (
  ),
   'subdir' => '/locales/vf_id',
   'translations' => 
  array (
    'id' => 
    array (
      0 => '/dash_core.php',
      1 => '/dash_text.php',
      2 => '/definitions.php',
      3 => '/missing.php',
      4 => '/site_core.php',
    ),
  ),
   'special' => 
  array (
  ),
));
