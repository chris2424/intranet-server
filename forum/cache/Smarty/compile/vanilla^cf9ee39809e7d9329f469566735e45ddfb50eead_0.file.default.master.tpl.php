<?php
/* Smarty version 3.1.33, created on 2019-07-26 09:49:49
  from 'L:\Xampp\htdocs\BNPBcenter\themes\keystone\views\default.master.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5d3accbddbba82_61535221',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'cf9ee39809e7d9329f469566735e45ddfb50eead' => 
    array (
      0 => 'L:\\Xampp\\htdocs\\BNPBcenter\\themes\\keystone\\views\\default.master.tpl',
      1 => 1564134586,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5d3accbddbba82_61535221 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_checkPlugins(array(0=>array('file'=>'L:\\Xampp\\htdocs\\BNPBcenter\\library\\SmartyPlugins\\function.asset.php','function'=>'smarty_function_asset',),1=>array('file'=>'L:\\Xampp\\htdocs\\BNPBcenter\\library\\SmartyPlugins\\function.home_link.php','function'=>'smarty_function_home_link',),2=>array('file'=>'L:\\Xampp\\htdocs\\BNPBcenter\\library\\SmartyPlugins\\function.logo.php','function'=>'smarty_function_logo',),3=>array('file'=>'L:\\Xampp\\htdocs\\BNPBcenter\\library\\SmartyPlugins\\function.mobile_logo.php','function'=>'smarty_function_mobile_logo',),4=>array('file'=>'L:\\Xampp\\htdocs\\BNPBcenter\\library\\SmartyPlugins\\function.categories_link.php','function'=>'smarty_function_categories_link',),5=>array('file'=>'L:\\Xampp\\htdocs\\BNPBcenter\\library\\SmartyPlugins\\function.discussions_link.php','function'=>'smarty_function_discussions_link',),6=>array('file'=>'L:\\Xampp\\htdocs\\BNPBcenter\\library\\SmartyPlugins\\function.custom_menu.php','function'=>'smarty_function_custom_menu',),7=>array('file'=>'L:\\Xampp\\htdocs\\BNPBcenter\\library\\SmartyPlugins\\function.module.php','function'=>'smarty_function_module',),8=>array('file'=>'L:\\Xampp\\htdocs\\BNPBcenter\\library\\SmartyPlugins\\function.t.php','function'=>'smarty_function_t',),9=>array('file'=>'L:\\Xampp\\htdocs\\BNPBcenter\\library\\SmartyPlugins\\function.activity_link.php','function'=>'smarty_function_activity_link',),10=>array('file'=>'L:\\Xampp\\htdocs\\BNPBcenter\\library\\SmartyPlugins\\function.searchbox.php','function'=>'smarty_function_searchbox',),11=>array('file'=>'L:\\Xampp\\htdocs\\BNPBcenter\\library\\SmartyPlugins\\function.follow_button.php','function'=>'smarty_function_follow_button',),12=>array('file'=>'L:\\Xampp\\htdocs\\BNPBcenter\\library\\SmartyPlugins\\function.homepage_title.php','function'=>'smarty_function_homepage_title',),13=>array('file'=>'L:\\Xampp\\htdocs\\BNPBcenter\\library\\SmartyPlugins\\function.breadcrumbs.php','function'=>'smarty_function_breadcrumbs',),14=>array('file'=>'L:\\Xampp\\htdocs\\BNPBcenter\\vendor\\smarty\\smarty\\libs\\plugins\\modifier.date_format.php','function'=>'smarty_modifier_date_format',),15=>array('file'=>'L:\\Xampp\\htdocs\\BNPBcenter\\library\\SmartyPlugins\\function.event.php','function'=>'smarty_function_event',),));
?>
<!DOCTYPE html>
<html lang="<?php echo $_smarty_tpl->tpl_vars['CurrentLocale']->value['Key'];?>
">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <?php echo smarty_function_asset(array('name'=>"Head"),$_smarty_tpl);?>

    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,400i,700,700i" rel="stylesheet">
</head>

<?php $_smarty_tpl->_assignInScope('linkFormat', "<div class='Navigation-linkContainer'>
        <a href='%url' class='Navigation-link %class'>
            %text
        </a>
    </div>");
$_smarty_tpl->_assignInScope('SectionGroups', (isset($_smarty_tpl->tpl_vars['Groups']->value) || isset($_smarty_tpl->tpl_vars['Group']->value)));?>

<body id="<?php echo $_smarty_tpl->tpl_vars['BodyID']->value;?>
" class="
    <?php echo $_smarty_tpl->tpl_vars['BodyClass']->value;?>


    <?php if ($_smarty_tpl->tpl_vars['ThemeOptions']->value['Options']['hasHeroBanner']) {?>
        ThemeOptions-hasHeroBanner
    <?php }?>

    <?php if ($_smarty_tpl->tpl_vars['ThemeOptions']->value['Options']['hasFeatureSearchbox']) {?>
        ThemeOptions-hasFeatureSearchbox
    <?php } else { ?>
        hideHomepageTitle
    <?php }?>

    <?php if ($_smarty_tpl->tpl_vars['ThemeOptions']->value['Options']['panelToLeft']) {?>
        ThemeOptions-panelToLeft
    <?php }?>

    <?php if ($_smarty_tpl->tpl_vars['User']->value['SignedIn']) {?>
        UserLoggedIn
    <?php } else { ?>
        UserLoggedOut
    <?php }?>

    <?php if (inSection('Discussion') && $_smarty_tpl->tpl_vars['Page']->value > 1) {?>
        isNotFirstPage
    <?php }?>

    <?php if (inSection('Group') && !isset($_smarty_tpl->tpl_vars['Group']->value['Icon'])) {?>
        noGroupIcon
    <?php }?>

    locale-<?php echo $_smarty_tpl->tpl_vars['CurrentLocale']->value['Lang'];?>

">

    <!--[if lt IE 9]>
      <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->

    <div class="Frame" id="page">
        <div class="Frame-top">
            <div class="Frame-header">

                <!---------- Main Header ---------->
                <header id="MainHeader" class="Header">
                    <div class="Container">
                        <div class="row">
                            <div class="Hamburger">
                                <button class="Hamburger Hamburger-menuXcross" id="menu-button" aria-label="toggle menu">
                                    <span class="Hamburger-menuLines" aria-hidden="true">
                                    </span>
                                    <span class="Hamburger-visuallyHidden sr-only">
                                        toggle menu
                                    </span>
                                </button>
                            </div>
                            <a href="<?php echo smarty_function_home_link(array('format'=>"%url"),$_smarty_tpl);?>
" class="Header-logo">
                                <?php echo smarty_function_logo(array(),$_smarty_tpl);?>

                            </a>
                            <a href="<?php echo smarty_function_home_link(array('format'=>"%url"),$_smarty_tpl);?>
" class="Header-logo mobile">
                                <?php echo smarty_function_mobile_logo(array(),$_smarty_tpl);?>

                            </a>
                            <nav class="Header-desktopNav">
                                <?php echo smarty_function_categories_link(array('format'=>$_smarty_tpl->tpl_vars['linkFormat']->value),$_smarty_tpl);?>

                                <?php echo smarty_function_discussions_link(array('format'=>$_smarty_tpl->tpl_vars['linkFormat']->value),$_smarty_tpl);?>

                                <?php echo smarty_function_custom_menu(array('format'=>$_smarty_tpl->tpl_vars['linkFormat']->value),$_smarty_tpl);?>

                            </nav>
                            <div class="Header-flexSpacer"></div>
                            <div class="Header-right">
                                <div class="MeBox-header">
                                    <?php echo smarty_function_module(array('name'=>"MeModule",'CssClass'=>"FlyoutRight"),$_smarty_tpl);?>

                                </div>
                                <?php if ($_smarty_tpl->tpl_vars['User']->value['SignedIn']) {?>
                                    <button class="mobileMeBox-button">
                                        <span class="Photo PhotoWrap">
                                            <img src="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['User']->value['Photo'], ENT_QUOTES, 'UTF-8', true);?>
" class="ProfilePhotoSmall" alt="<?php echo smarty_function_t(array('c'=>'Avatar'),$_smarty_tpl);?>
">
                                        </span>
                                    </button>
                                <?php }?>
                            </div>
                        </div>
                    </div>

                    <!---------- Mobile Navigation ---------->
                    <nav class="Navigation js-nav needsInitialization">
                        <div class="Container">
                            <?php if ($_smarty_tpl->tpl_vars['User']->value['SignedIn']) {?>
                                <div class="Navigation-row NewDiscussion">
                                    <div class="NewDiscussion mobile">
                                        <?php echo smarty_function_module(array('name'=>"NewDiscussionModule"),$_smarty_tpl);?>

                                    </div>
                                </div>
                            <?php } else { ?>
                                <div class="Navigation-row">
                                    <div class="SignIn mobile">
                                        <?php echo smarty_function_module(array('name'=>"MeModule"),$_smarty_tpl);?>

                                    </div>
                                </div>
                            <?php }?>
                            <?php echo smarty_function_categories_link(array('format'=>$_smarty_tpl->tpl_vars['linkFormat']->value),$_smarty_tpl);?>

                            <?php echo smarty_function_discussions_link(array('format'=>$_smarty_tpl->tpl_vars['linkFormat']->value),$_smarty_tpl);?>

                            <?php echo smarty_function_activity_link(array('format'=>$_smarty_tpl->tpl_vars['linkFormat']->value),$_smarty_tpl);?>

                            <?php echo smarty_function_custom_menu(array('format'=>$_smarty_tpl->tpl_vars['linkFormat']->value),$_smarty_tpl);?>

                        </div>
                    </nav>
                    <nav class="mobileMebox js-mobileMebox needsInitialization">
                        <div class="Container">
                            <?php echo smarty_function_module(array('name'=>"MeModule"),$_smarty_tpl);?>

                            <button class="mobileMebox-buttonClose Close">
                                <span>×</span>
                            </button>
                        </div>
                    </nav>
                    <!---------- Mobile Navigation END ---------->

                </header>
                <!---------- Main Header END ---------->

            </div>
            <div class="Frame-body">

                <!---------- Hero Banner ---------->
                <?php if ($_smarty_tpl->tpl_vars['ThemeOptions']->value['Options']['hasHeroBanner'] && inSection(array("CategoryList","DiscussionList"))) {?>
                    <div class="Herobanner">
                        <?php if ($_smarty_tpl->tpl_vars['heroImageUrl']->value) {?>
                            <div class="Herobanner-bgImage" style="background-image:url('<?php echo $_smarty_tpl->tpl_vars['heroImageUrl']->value;?>
')"></div>
                        <?php }?>
                        <div class="Container">
                            <?php if ($_smarty_tpl->tpl_vars['ThemeOptions']->value['Options']['hasFeatureSearchbox']) {?>
                                <div class="SearchBox js-sphinxAutoComplete" role="search">
                                    <?php if ($_smarty_tpl->tpl_vars['hasAdvancedSearch']->value === true) {?>
                                        <?php echo smarty_function_module(array('name'=>"AdvancedSearchModule"),$_smarty_tpl);?>

                                    <?php } else { ?>
                                        <?php echo smarty_function_searchbox(array(),$_smarty_tpl);?>

                                    <?php }?>
                                </div>
                            <?php } else { ?>
                                <?php if ($_smarty_tpl->tpl_vars['Category']->value) {?>
                                    <h2 class="H HomepageTitle"><?php echo $_smarty_tpl->tpl_vars['Category']->value['Name'];
echo smarty_function_follow_button(array(),$_smarty_tpl);?>
</h2>
                                    <p class="P PageDescription"><?php echo $_smarty_tpl->tpl_vars['Category']->value['Description'];?>
</p>
                                <?php } else { ?>
                                    <?php ob_start();
echo smarty_function_homepage_title(array(),$_smarty_tpl);
$_prefixVariable1 = ob_get_clean();
if ($_prefixVariable1 !== '') {?>
                                        <h2 class="H HomepageTitle"><?php echo smarty_function_homepage_title(array(),$_smarty_tpl);?>
</h2>
                                    <?php }?>
                                    <?php if ($_smarty_tpl->tpl_vars['_Description']->value) {?>
                                        <p class="P PageDescription"><?php echo $_smarty_tpl->tpl_vars['_Description']->value;?>
</p>
                                    <?php }?>
                                <?php }?>
                            <?php }?>
                        </div>
                    </div>
                <?php }?>
                <!---------- Hero Banner END ---------->

                <div class="Frame-content">
                    <div class="Container">
                        <div class="Frame-contentWrap">
                            <div class="Frame-details">
                                <?php if (!$_smarty_tpl->tpl_vars['isHomepage']->value) {?>
                                    <div class="Frame-row">
                                        <nav class="BreadcrumbsBox">
                                            <?php echo smarty_function_breadcrumbs(array(),$_smarty_tpl);?>

                                        </nav>
                                    </div>
                                <?php }?>
                                <div class="Frame-row SearchBoxMobile">
                                    <?php if (!$_smarty_tpl->tpl_vars['SectionGroups']->value && !inSection(array("SearchResults"))) {?>
                                        <div class="SearchBox js-sphinxAutoComplete" role="search">
                                            <?php if ($_smarty_tpl->tpl_vars['hasAdvancedSearch']->value === true) {?>
                                                <?php echo smarty_function_module(array('name'=>"AdvancedSearchModule"),$_smarty_tpl);?>

                                            <?php } else { ?>
                                                <?php echo smarty_function_searchbox(array(),$_smarty_tpl);?>

                                            <?php }?>
                                        </div>
                                    <?php }?>
                                </div>
                                <div class="Frame-row">

                                    <!---------- Main Content ---------->
                                    <main class="Content MainContent">
                                        <!---------- Profile Page Header ---------->
                                        <?php if (inSection("Profile")) {?>
                                            <div class="Profile-header">
                                                <div class="Profile-photo">
                                                    <div class="PhotoLarge">
                                                        <?php echo smarty_function_module(array('name'=>"UserPhotoModule"),$_smarty_tpl);?>

                                                    </div>
                                                </div>
                                                <div class="Profile-name">
                                                    <div class="Profile-row">
                                                        <h1 class="Profile-username">
                                                            <?php echo $_smarty_tpl->tpl_vars['Profile']->value['Name'];?>

                                                        </h1>
                                                    </div>
                                                    <div class="Profile-row">
                                                        <?php if (isset($_smarty_tpl->tpl_vars['Rank']->value)) {?>
                                                            <span class="Profile-rank"><?php echo $_smarty_tpl->tpl_vars['Rank']->value['Label'];?>
</span>
                                                        <?php }?>
                                                    </div>
                                                </div>
                                            </div>
                                        <?php }?>
                                        <!---------- Profile Page Header END ---------->

                                        <?php echo smarty_function_asset(array('name'=>"Content"),$_smarty_tpl);?>

                                    </main>
                                    <!---------- Main Content END ---------->

                                    <!---------- Main Panel ---------->
                                    <aside class="Panel Panel-main">
                                        <?php if (!$_smarty_tpl->tpl_vars['SectionGroups']->value) {?>
                                            <div class="SearchBox js-sphinxAutoComplete" role="search">
                                                <?php echo smarty_function_searchbox(array(),$_smarty_tpl);?>

                                            </div>
                                        <?php }?>
                                        <?php echo smarty_function_asset(array('name'=>"Panel"),$_smarty_tpl);?>

                                    </aside>
                                    <!---------- Main Panel END ---------->

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="Frame-footer">

            <!---------- Main Footer END ---------->
            <footer class="Footer">
                <div class="Container">
                    <div class="row">
                        <div class="col">
                            <p class="Footer-copyright"><?php echo smarty_function_t(array('c'=>"© BNPB"),$_smarty_tpl);?>
 <?php echo smarty_modifier_date_format(time(),"%Y");?>
</p>
                        </div>
                        <div class="col">
                             <div class="Vanilla-logo">
                            <a href="https://bnpb.go.id"><img  class="img-fluid" width="100" src="http://perpustakaan.bnpb.go.id/template/bnpblib/assets/img/bnpb-footer-logo.png" alt=""></a> 
                        </div>
                        </div>
                    </div>
                    <?php echo smarty_function_asset(array('name'=>"Foot"),$_smarty_tpl);?>

                </div>
            </footer>
            <!---------- Main Footer END ---------->

        </div>
    </div>
    <div id="modals"></div>
    <?php echo smarty_function_event(array('name'=>"AfterBody"),$_smarty_tpl);?>

</body>

</html>
<?php }
}
