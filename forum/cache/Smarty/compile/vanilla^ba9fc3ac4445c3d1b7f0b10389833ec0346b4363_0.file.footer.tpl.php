<?php
/* Smarty version 3.1.33, created on 2019-07-26 09:59:51
  from 'L:\Xampp\htdocs\BNPBcenter\themes\theme-boilerplate\views\partials\footer.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5d3acf1757e3b4_00110333',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'ba9fc3ac4445c3d1b7f0b10389833ec0346b4363' => 
    array (
      0 => 'L:\\Xampp\\htdocs\\BNPBcenter\\themes\\theme-boilerplate\\views\\partials\\footer.tpl',
      1 => 1564135181,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5d3acf1757e3b4_00110333 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_checkPlugins(array(0=>array('file'=>'L:\\Xampp\\htdocs\\BNPBcenter\\library\\SmartyPlugins\\function.t.php','function'=>'smarty_function_t',),1=>array('file'=>'L:\\Xampp\\htdocs\\BNPBcenter\\vendor\\smarty\\smarty\\libs\\plugins\\modifier.date_format.php','function'=>'smarty_modifier_date_format',),2=>array('file'=>'L:\\Xampp\\htdocs\\BNPBcenter\\library\\SmartyPlugins\\function.asset.php','function'=>'smarty_function_asset',),));
?>
<footer class="Footer">
    <div class="Container">
        <div class="row">
            <div class="col">
                <p class="Footer-copyright"><?php echo smarty_function_t(array('c'=>"© Pusat Data Informasi Dan Humas"),$_smarty_tpl);?>
 <?php echo smarty_modifier_date_format(time(),"%Y");?>
</p>
            </div>
            <div class="col">
                <div class="Vanilla-logo">
                            <a href="https://bnpb.go.id"><img  class="img-fluid" width="100" src="http://perpustakaan.bnpb.go.id/template/bnpblib/assets/img/bnpb-footer-logo.png" alt=""></a> 
                        </div>
            </div>
        </div>
        <?php echo smarty_function_asset(array('name'=>"Foot"),$_smarty_tpl);?>

    </div>
</footer>
<?php }
}
