<?php if (!defined('APPLICATION')) exit();

$PluginInfo['UsersOnline'] = array(
	'Name' => 'Users Online',
   'Description' => 'Displays total amount of users browsing (logged in or out) in the last \'x\' minutes.',
   'Version' => '1.1',
   'RequiredTheme' => FALSE, 
   'RequiredPlugins' => FALSE,
   'HasLocale' => FALSE,
   'SettingsUrl' => '/plugin/usersonline',
   'SettingsPermission' => 'Garden.AdminUser.Only',
   'Author' => "ImWithStupid"
);

class UsersOnlinePlugin extends Gdn_Plugin {

   /**
    * Get instance of UsersOnlineModel.
    *
    * @return object UsersOnlineModel
    */
   public function UsersOnlineModel() {
      static $UsersOnlineModel = NULL;

      if ($UsersOnlineModel === NULL) {
         $UsersOnlineModel = new UsersOnlineModel();
      }
      return $UsersOnlineModel;
   }

   /**
    * DiscussionsController_Render_After Event Hook
    * @param $Sender Sending controller instance
    *
    * DiscussionsController_AfterRenderAsset_Handler($Sender) *Use based on preference*
    */ 
   public function DiscussionsController_Render_After($Sender) {
      // Get decimal of user's ip address.
      $UserIp = sprintf("%u", ip2long($_SERVER['REMOTE_ADDR']));
      $Time = time();
      $ExpLength = intval(ltrim(Gdn::Config('Plugin.UsersOnline.ExpLength', 'num5'), 'num'));
      $SessionExpTime = $Time - ($ExpLength * 60);
      
      // Update user's last visited time with current time or insert new session.
      $this->UsersOnlineModel()->SetSession($UserIp, $Time);
      
      // Delete session(s) older than expiration time.
     $this->UsersOnlineModel()->DeleteSessions($SessionExpTime);
      
      // Return total active sessions.
      $TotalSessions = $this->UsersOnlineModel()->GetTotalSessions();
      
      $MsgTxt = Gdn::Config('Plugin.UsersOnline.MsgTxt', '');
      echo '<div id="usersOnline">'.$TotalSessions.' '.$MsgTxt.'</div>';
   }

   /**
    * CategoriesController_Render_After Event Hook
    * @param $Sender Sending controller instance
    *
    * CategoriesController_AfterRenderAsset_Handler($Sender) *Use based on preference*
    * $this->DiscussionsController_AfterRenderAsset_Handler($Sender) *Use based on preference*
    */
   public function CategoriesController_Render_After($Sender) {
      $this->DiscussionsController_Render_After($Sender);
   }
   
   /**
    * DiscussionsController_Render_Before Event Hook
    * @param $Sender Sending controller instance
    */
   public function DiscussionsController_Render_Before($Sender) {
      $Sender->AddCssFile($this->GetResource('design/usersonline.css', FALSE, FALSE));
   }
   
   /**
    * CategoriesController_Render_Before Event Hook
    * @param $Sender Sending controller instance
    */
   public function CategoriesController_Render_Before($Sender) {
      $this->DiscussionsController_Render_Before($Sender);
   }
   
   /**
    * Create a method called "UsersOnline" on the PluginController
    *
    * @param $Sender Sending controller instance
    */
   public function PluginController_UsersOnline_Create($Sender) {
      $Sender->Title('Users Online Plugin');
      $Sender->AddSideMenu('plugin/usersonline');
      $Sender->Form = new Gdn_Form();
      $this->Dispatch($Sender, $Sender->RequestArgs);      
   }
   
   public function Controller_Index($Sender) {
      // Prevent non-admins from accessing this page
      $Sender->Permission('Vanilla.Settings.Manage');
      
      $Sender->SetData('PluginDescription',$this->GetPluginKey('Description'));
		
		$Validation = new Gdn_Validation();
      $ConfigurationModel = new Gdn_ConfigurationModel($Validation);
      $ConfigurationModel->SetField(array(
         'Plugin.UsersOnline.ExpLength'    => 'num5',
         'Plugin.UsersOnline.MsgTxt'       => 'users online'
      ));
      
      // Set the model on the form.
      $Sender->Form->SetModel($ConfigurationModel);
   
      // If seeing the form for the first time...
      if ($Sender->Form->AuthenticatedPostBack() === FALSE) {
         // Apply the config settings to the form.
         $Sender->Form->SetData($ConfigurationModel->Data);
		} else {
         $ConfigurationModel->Validation->ApplyRule('Plugin.UsersOnline.ExpLength', 'Required');
         $ConfigurationModel->Validation->ApplyRule('Plugin.UsersOnline.MsgTxt', 'Required');
         
         $Saved = $Sender->Form->Save();
         if ($Saved) {
            $Sender->StatusMessage = T("Your changes have been saved.");
         }
      }
      
      // GetView() looks for files inside plugins/PluginFolderName/views/ and returns their full path. Useful!
      $Sender->Render($this->GetView('usersonline.php'));
   }
   
   public function Base_GetAppSettingsMenuItems_Handler($Sender) {
      $Menu = &$Sender->EventArguments['SideMenu'];
      $Menu->AddLink('Add-ons', 'Users Online', 'plugin/usersonline', 'Garden.AdminUser.Only');
   }
   
   /**
    * Plugin setup
    */
   public function Setup() {
      SaveToConfig('Plugin.UsersOnline.ExpLength', 'num5');
      SaveToConfig('Plugin.UsersOnline.MsgTxt', 'users online');
      
      // Create table GDN_UsersOnline, if it doesn't already exist
      Gdn::Structure()
         ->Table('UsersOnline')
         ->PrimaryKey('SessionID')
         ->Column('UserIp', 'uint')
         ->Column('Time', 'uint', 0)
         ->Set(FALSE, FALSE);
   }

   public function OnDisable() {
      RemoveFromConfig('Plugin.UsersOnline.ExpLength');
      RemoveFromConfig('Plugin.UsersOnline.MsgTxt');
      
      // Delete table GDN_UsersOnline
      Gdn::Structure()->Table('UsersOnline')->Drop();
   }
}
