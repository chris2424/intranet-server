<?php if (!defined('APPLICATION')) exit();

class UsersOnlineModel extends Gdn_Model {

   /**
    * Update existing session with new Time or insert new session.
    *
    * @param int $UserIp The decimal ip address of the user.
    * @param int $Time The current time using time().
    */ 
   public function SetSession($UserIp, $Time) {
      $Set = array('Time' => $Time);
      $Where = array('UserIp' => $UserIp);
		return Gdn::SQL()->Replace('UsersOnline', $Set, $Where, TRUE);   
   }

   /**
    * Delete session(s) where Time is older than 10 minutes.
    *
    * @param int $SessionExpTime The current time offset by the amount of time to expiration.
    */   
   public function DeleteSessions($SessionExpTime) {
      $Where = array('Time <' => $SessionExpTime);
      return Gdn::SQL()->Delete('UsersOnline', $Where);
   }

   /**
    * Count total number of sessions in table.
    *
    * @return int The result of COUNT(*) select query.
    */   
   public function GetTotalSessions() {
		return Gdn::SQL()->GetCount('UsersOnline');
   }
}