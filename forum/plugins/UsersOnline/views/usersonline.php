<?php if (!defined('APPLICATION')) exit(); ?>

<h1><?php echo T($this->Data['Title']); ?></h1>
<div class="Info">
   <?php echo T($this->Data['PluginDescription']); ?>
</div>
<h3><?php echo T('Settings'); ?></h3>
<?php
   echo $this->Form->Open();
   echo $this->Form->Errors();
?>
<ul>
   <li><?php
      echo $this->Form->Label('Include users active in last', 'Plugin.UsersOnline.ExpLength');
      echo $this->Form->DropDown('Plugin.UsersOnline.ExpLength',array(
         'num1'     => '1',
         'num5'     => '5',
         'num10'    => '10',
         'num15'    => '15',
         'num20'    => '20',
         'num30'    => '30'
      ));
      echo ' min(s)';
   ?></li>
   <li><?php
      echo $this->Form->Label('Message', 'Plugin.UsersOnline.MsgTxt');
      echo $this->Form->Textbox('Plugin.UsersOnline.MsgTxt');
   ?></li>
</ul>
<?php
   echo $this->Form->Close('Save');
?>