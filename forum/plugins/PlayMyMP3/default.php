<?php if (!defined('APPLICATION')) exit();

$PluginInfo['PlayMyMP3'] = array(
    'Name' => 'PlayMyMP3',
    'Description' => "Adds a Play Button to each mp3 link you post so you can listen to them right in the page. Uses 1 Bit Audio Player: http://1bit.markwheeler.net, based on Play Mp3 Links plugin. You must have adobe flash in your browser.Works with 2.3.",
    'Version' => '1.1',
    'License'=>"GNU GPL2",
   'Author' => "VrijVlinder",
);

class PlayMP3LinksPlugin extends Gdn_Plugin {

    public function Base_Render_Before($Sender) {
        $Sender->AddCssFile('pml.css', 'plugins/PlayMyMP3');
        $Sender->AddJsFile('swfobject.js','plugins/PlayMyMP3');
        $Sender->AddJsFile('1bit.js', 'plugins/PlayMyMP3');
		$js_insert = '
<script type="text/javascript">
	oneBit = new OneBit("'.Url('plugins/PlayMyMP3/1bit.swf').'");
	oneBit.ready(function() {
		oneBit.specify("playerSize", "25");
		oneBit.apply("a");
	});
</script>
';
		$Sender->Head->AddString($js_insert);
    }
    
    public function Setup() {}
}
