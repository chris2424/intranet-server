<?php if (!defined('APPLICATION')) exit();

// 2.0.4 - mosullivan:
// Removed deprecated function call. 
// Corrected css reference. 
// Fixed a bug that caused emoticons to not open the dropdown when editing.
// Cleaned up plugin to reference itself properly, allowing for multiple linestickers's on a single page to work together.
$PluginInfo['linestickers'] = array(
	'Name' => 'Line Stickers',
	'Description' => 'Replaces <a href="http://en.wikipedia.org/wiki/Emoticon">emoticons</a> (smilies) with Line Stickers.',
	'Version' 	=>	 '2.0.6',
	'MobileFriendly' => TRUE,
	'Author' 	=>	 "Afif Ulinuha | Nexus Informatics Studio's",
	'AuthorEmail' => 'hello@nexusinfostudio.com.com',
	'AuthorUrl' =>	 'http://nexusinfostudio.com',
	'Special Thanks' 	=>	 "Mark O'Sullivan",
	'License' => 'GPL v2',
	'RequiredApplications' => array('Vanilla' => '>=2.0.18'),
);

/**
 * Note: Added jquery events required for proper display/hiding of emoticons
 * as write & preview buttons are clicked on forms in Vanilla 2.0.14. These
 * are necessary in order for this plugin to work properly.
 */

class linestickersPlugin implements Gdn_IPlugin {
   
   public function AssetModel_StyleCss_Handler($Sender) {
      $Sender->AddCssFile('linestickers.css', 'plugins/linestickers');
   }
	
	/**
	 * Replace emoticons in comments.
	 */
	public function Base_AfterCommentFormat_Handler($Sender) {
		if (!C('Plugins.linestickers.FormatEmoticons', TRUE))
			return;

		$Object = $Sender->EventArguments['Object'];
		$Object->FormatBody = $this->DoEmoticons($Object->FormatBody);
		$Sender->EventArguments['Object'] = $Object;
	}
	
	public function DiscussionController_Render_Before($Sender) {
		$this->_linestickersSetup($Sender);
	}

	/**
	 * Return an array of emoticons.
	 */
	public static function GetEmoticons() {
		return array(
			':thankyou' => '100',
			':beautiful' => '71',
			':goodbye' => '67',
			':stop' => '21',
			':sukses' => '1',
			':mantap' => '51',
			':wink' => '20',
			':capedeh' => '52',
			':capede' => '52',
			':mikirpakeotak' => '2',
			':gatau' => '3',
			':hmm' => '4',
			':hm' => '4',
			':sinis' => '5',
			':ngakak' => '6',
			':kiss' => '7',
			':timetoeat' => '8',
			':bagak' => '9',
			':gelambir' => '10',
         ':mantapgilak' => '36',
			':bringit' => '11',
			':cemberut' => '12',
			':heran' => '13',
			':kaget' => '34',
			':naikpitam' => '15',
			':naikpitam2' => '16',
			':akutakpercaya' => '14',
			':hopeless' => '17',
			':cry' => '18',
			':mewek' => '19',
			':kalah' => '33',
			':sad' => '22',
			':sedih' => '22',
			':esmosi' => '23',
			':esmosi2' => '24',
			':cool' => '25',
			':error' => '26',
			':tabah' => '27',
			':belajar' => '28',
			':rekeningkosong' => '32',
			':siangsunyi' => '29',
			':soresunyi' => '30',
			':malamsunyi' => '31',
			
			
			':nampungaer' => '35',
			':meratapi' => '37',
			':meratapi2' => '38',
			
			':digunjing' => '39',
			':nasehat' => '40',
			':terimakasih' => '41',
			':php' => '42',
			':calling' => '43',
			':bertekat' => '44',
			':selamat' => '45',
			':monggo' => '46',
			':sukses2' => '47',
			':sukses3' => '48',
			':berhasil' => '49',
			':selfie' => '50',		
			':brownsukses' => '53',
		':gugup' => '54',
			':alone' => '55',
			':blank' => '56',
			':berteduh' => '57',
			':think' => '58',
			':clumsy' => '59',
		':lunch' => '60',
					':brushgigi' => '72',
			':santai' => '61',
				':semangat2' => '69',
			':punch' => '62',
			':shot' => '63',
			':piknik' => '64',
			':tangkap' => '65',
			':pulang' => '66',
			':buru' => '68',
		
			':Gowes' => '70',
			':brushgigi' => '72',

			':cony' => '73',
			':matabelo' => '74',
			':ganbatte' => '75',
			':ngakak2' => '76',
			':gugup2' => '77',
			':terharu' => '78',
			':conycry' => '79',
			':romantic' => '101',
			':teler' => '102',
			':acuh' => '103',
			':kesal' => '104',
			':omae' => '105',
			':semangat3' => '106',
			':belajarketat' => '107',
			':dietketat' => '108',
			':outofenergy' => '109',
			':stalker' => '110',
			':Stalker' => '110',
			':ngadem' => '111',
			':conyalone' => '112',
			':konfirmasi' => '113',
			':cek' => '113',
			':conytkp' => '114',
			':okay' => '115',
			
			':fireworks' => 'pirate'

		);
	}
	
	/**
	 * Replace emoticons in comment preview.
	 */
	public function PostController_AfterCommentPreviewFormat_Handler($Sender) {
		if (!C('Plugins.linestickers.FormatEmoticons', TRUE))
			return;
		
		$Sender->Comment->Body = $this->DoEmoticons($Sender->Comment->Body);
	}
	
	public function PostController_Render_Before($Sender) {
		$this->_linestickersSetup($Sender);
	}
   
   public function NBBCPlugin_AfterNBBCSetup_Handler($Sender, $Args) {
//      $BBCode = new BBCode();
      $BBCode = $Args['BBCode'];
      $BBCode->smiley_url = SmartAsset('/plugins/linestickers/design/images');
      
      $Smileys = array();
      foreach (self::GetEmoticons() as $Text => $Filename) {
         $Smileys[$Text]= $Filename.'.gif';
      }
      
      $BBCode->smileys = $Smileys;
   }
	
	/**
	 * Thanks to punbb 1.3.5 (GPL License) for this function - ported from their do_smilies function.
	 */
	public static function DoEmoticons($Text) {
		$Text = ' '.$Text.' ';
		$Emoticons = linestickersPlugin::GetEmoticons();
		foreach ($Emoticons as $Key => $Replacement) {
			if (strpos($Text, $Key) !== FALSE)
				$Text = preg_replace(
					"#(?<=[>\s])".preg_quote($Key, '#')."(?=\W)#m",
					'<span class="Emoticon Emoticon' . $Replacement . '"><span>' . $Key . '</span></span>',
					$Text
				);
		}

		return substr($Text, 1, -1);
	}

	/**
	 * Prepare a page to be emotified.
	 */
	private function _linestickersSetup($Sender) {
		$Sender->AddJsFile('linestickers.js', 'plugins/linestickers');  
		// Deliver the emoticons to the page.
      $Emoticons = array();
      foreach ($this->GetEmoticons() as $i => $gif) {
         if (!isset($Emoticons[$gif]))
            $Emoticons[$gif] = $i;
      }
      $Emoticons = array_flip($Emoticons);
      
		$Sender->AddDefinition('Emoticons', base64_encode(json_encode($Emoticons)));
	}
	
	public function Setup() {
		//SaveToConfig('Plugins.linestickers.FormatEmoticons', TRUE);
		SaveToConfig('Garden.Format.Hashtags', FALSE); // Autohashing to search is incompatible with linestickers
	}
	
}