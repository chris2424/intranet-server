<?php if (!defined('APPLICATION')) exit();
	// Define the plugin:


	$PluginInfo['Galleries'] = array(  
	 'Name' => 'Galleries', 
	 'Description' => "Adds an Image Gallery to your forum .<br/>Users can creat their own albums, or you can create a community album for everyone.",
	 'Version' => '2.1',
     'Author' => "VrijVlinder", 
     'AuthorEmail' => 'contact@vrijvlinder.com',   
     'AuthorUrl' => 'http://www.vrijvlinder.com',
     'License'=>"GNU GPL2",
     'RegisterPermissions' => array('Plugins.Attachments.Upload.Allow'));
	
	class Galleries extends Gdn_Plugin {
		
		public function Base_Render_Before($Sender) {
			$Session = Gdn::Session();
			if ($Sender->Menu){
				$Sender->Menu->AddLink('Gallery',T('Gallery'), 'gallery', array('Plugins.Attachments.Upload.Allow'));
				
			

		}
	}
        
        
		public function PluginController_Gallery_Create($Sender) {
			$Sender->ClearCssFiles();
			$Sender->AddCssFile('style.css');
			$Sender->RemoveCssFile('admin.css');
			$Sender->AddCssFile('gallery.css','plugins/Galleries');
			$Sender->AddCssFile('fancybox.css','plugins/Galleries');
			$Sender->AddJsFile('fancybox.js','plugins/Galleries');
			$Sender->AddJsFile('jbox.js','plugins/Galleries');
			$Sender->MasterView = 'default';
			$Sender->Render($this->GetView('gallery.php'));
			
		
		}

		public function Setup() {
			Gdn::Router()->SetRoute('gallery','plugin/gallery','Internal');
			
			if(!is_dir('uploads/picgal/')) $go = mkdir('uploads/picgal/', 0775);
		}

		public function OnDisable() {
			SaveToConfig('EnabledPlugins.Galleries', FALSE);
		}

	}
