jQuery(document).ready(function($){
		$(".fancybox").fancybox({
				wrapCSS    : 'fancybox-custom',
				closeClick : true,

				helpers : {
					title : {
						type : 'inside'
					},
					overlay : {
						css : {
							'background-color' : '#000'
						}
					}
				}
			});
		});
