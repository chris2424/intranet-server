
Add this plugin to your plugin folder and enable from the dashboard. You first need to create a new Album and then the form appears in the Album you just created so you can upload pictures to it. Also can edit the names and delete them.

Only Logged in users can upload and make new folders. The link to the Gallery only appears to logged in users.

To erase the Albums, they are located in the uploads folder under uploads/picgal

To upload more than one at a time you can ftp to the specific folder and copy the names of the files into the uploads/picgal/yourgalleryalbumname/notes.dat.php  so they can display.

