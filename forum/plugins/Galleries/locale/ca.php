<?php if (!defined('APPLICATION')) exit();


$Definition['Picture Gallery'] = 'Picture Gallery';//Translation goes here
$Definition['Edit'] = 'Edit';
$Definition['Delete'] = 'Delete';
$Definition['Save'] = 'Save';
$Definition['Here you can add new Albums to the Gallery'] = 'Here you can add new Albums to the Gallery';
$Definition['Create new Album, Please Pick a Name - No Spaces'] = 'Create new Album, Please Pick a Name - No Spaces';
$Definition['Create new Album'] = 'Create new Album';
$Definition['Close'] = 'Close';
$Definition['Next'] = 'Next';
$Definition['Previous'] = 'Prev';
$Definition['Upload to'] = 'Upload to';
$Definition['Picture Caption Required'] = 'Picture Caption Required!';
$Definition['File'] = 'Image File';
$Definition['Upload New Picture'] = "Upload";
$Definition['Current Pictures In This Album'] = 'Current pictures in this album';
$Definition['Error'] = 'Error';
$Definition['F I N I S H E D'] = 'Done';
$Definition['Something happened to the directories permission !! ...'] = 'Something happened to the directories permission !! ...';
$Definition['The image size you uploaded was too big .... or'] = 'The image size you uploaded was too big .... or';
$Definition['Invalid Image format, supported image types are .jpg (.jpeg), .gif, and .png. -- '] = 'Invalid Image format, supported image types are .jpg (.jpeg), .gif, and .png. -- ';
$Definition['Sorry nothing was uploaded...'] = 'Sorry nothing was uploaded...';
$Definition['Can not save because the folder was not found...'] = 'Can not save because the folder was not found...';
$Definition['Some errors occured, The Name of the folder already exists....'] = 'Some errors occured, The Name of the folder already exists....';
$Definition['Here you can create new Albums'] = 'Here you can create new Albums';
$Definition['Go Back to main Gallery'] = 'Go Back to main Gallery';